# Hello there, I'm Emma 👋

Senior Product Analyst at GitLab supporting the Data Science Section. 📈 

If you are a team member at GitLab with access to Tableau, you can find Tableau resources for the Sections I support organized in [Collections](https://10az.online.tableau.com/#/site/gitlab/collections/allCollections).

## Key Facts 🔑

🌽 Born in Omaha, Nebraska, US ⛷️ Steamboat Springs, Colorado, US is home ⛰️ I love to spend time in nature. If we haven't met yet, set up a coffee chat with me ☕ 

